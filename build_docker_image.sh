echo "Working with registry: $REGISTRY_URL"
curl https://gitlab.com/KeyStorke/deployment/raw/master/DockerfileBuildNewImage > TMPDOCKERFILE
echo "Dockerfile has been downloaded"
eval "echo \" $(cat TMPDOCKERFILE)\"" > Dockerfile
echo "Dockerfile has been parsed"

curl -fsSL get.docker.com | sh
usermod -aG docker docker

sudo apt install apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt install docker-ce -y

echo "\n\nUser name is"
echo `whoami`
echo "\n\n"

echo "cat /etc/docker/daemon.json"
cat /etc/docker/daemon.json

echo
"sudo echo \"{
  \"storage-driver\": \"overlay2\"
}\" >> /etc/docker/daemon.json"

sudo echo "{
  \"storage-driver\": \"overlay2\"
}" >> /etc/docker/daemon.json

echo "cat /etc/docker/daemon.json"
cat /etc/docker/daemon.json


sudo dockerd &> /dev/null &
# sudo docker system prune

echo "sudo docker login -u xxxxxxx -p xxxxxxx $REGISTRY_URL \n\n"
sudo docker login -u "$REGISTRY_LOGIN_USERNAME" -p "$REGISTRY_LOGIN_PASSWORD" "$REGISTRY_URL"

echo "sudo docker pull registry.gitlab.com/keystorke/camera_agent:latest \n\n"
sudo docker pull registry.gitlab.com/keystorke/camera_agent:latest

echo "sudo docker build -t registry.gitlab.com/keystorke/camera_agent:latest . \n\n"
sudo docker build -t registry.gitlab.com/keystorke/camera_agent:latest .

echo "sudo docker push registry.gitlab.com/keystorke/camera_agent/agent:latest \n\n"
sudo docker push registry.gitlab.com/keystorke/camera_agent:latest
