echo `pwd`

project_path=`pwd`

echo "ls -a"
ls -a

echo "#"
echo "#"
echo "# call setup_utils.sh"
echo "#"
echo "#"
cd "$project_path"
bash deployment/setup_utils.sh

echo "#"
echo "#"
echo "# call install_golang.sh"
echo "#"
echo "#"
cd "$project_path"
bash deployment/install_golang.sh

echo "#"
echo "#"
echo "# call clone_project.sh"
echo "#"
echo "#"
cd "$project_path"
bash deployment/clone_project.sh

# echo "#"
# echo "#"
# echo "# call build_dlib.sh"
# echo "#"
# echo "#"
# cd "$project_path"
# bash deployment/build_dlib.sh

echo "#"
echo "#"
echo "# call install_go_face.sh"
echo "#"
echo "#"
cd "$project_path"
bash deployment/install_go_face.sh

echo "#"
echo "#"
echo "# call setup_amqp.sh"
echo "#"
echo "#"
cd "$project_path"
bash deployment/setup_amqp.sh

echo "#"
echo "#"
echo "# call setup_gocv.sh"
echo "#"
echo "#"
cd "$project_path"
bash deployment/setup_gocv.sh

echo "#"
echo "#"
echo "# call setup_logrus.sh"
echo "#"
echo "#"
cd "$project_path"
bash deployment/setup_logrus.sh

echo "#"
echo "#"
echo "# call install_opencv.sh"
echo "#"
echo "#"
bash deployment/install_opencv.sh

echo "#"
echo "#"
echo "# call install_goyaml.sh"
echo "#"
echo "#"
bash deployment/install_goyaml.sh

echo "#"
echo "#"
echo "# call install_infra.sh"
echo "#"
echo "#"
bash deployment/install_infra.sh

echo "#"
echo "#"
echo "# make"
echo "#"
echo "#"
cd ~/go/src/camera_agent
make
mv ~/go/src/camera_agent/binaries "$project_path"

cd "$project_path"
echo `pwd`
echo "ls -a"
ls -a

echo "ls -a binaries/"
ls -a binaries